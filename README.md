# Example to Demonstrate Issue with CMake with Visual Studio

Prerequisites:

* CMake
* Intel oneAPI
* Visual Studio

Steps to reproduce:

* Open Command Prompt
* `"\Program Files (x86)\Intel\oneAPI\setvars.bat"`
* `cmake -B build`
* Open VS -> open project or solution -> select `build\favor.sln`
* Click green arrow
* Right click RUN_TESTS -> select "Build"

Notes:

* The programs are successfully compiled, despite the error message that pops up
* Running the tests in VS fails, but running `ctest -C Debug` from the command prompt immediately after succeeds
